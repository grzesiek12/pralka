package pralka.java;

public abstract class Pralka {
	
	int			programMax	= 20;
	private int	actualProg	= 1;
	
	private double	maxTemp			= 90;
	private double	temp			= 0;
	double			tempRegulation	= 0.5;
	double			variableSetup	= 2.0;
	
	private int			maxWhirl	= 1000;
	private int			actualWhirl	= 0;
	private Producent	producent;
	
	public Producent getProducent() {
		return producent;
	}
	
	public void setProducent(Producent producent) {
		this.producent = producent;
	}
	
	public int getProgramMax() {
		return programMax;
	}
	
	public double getTempRegulation() {
		return tempRegulation;
	}
	
	public int getActualWhirl() {
		return actualWhirl;
	}
	
	public void setActualWhirl(int actualWhirl) {
		if (actualWhirl <= maxWhirl && actualWhirl >= 0)
			this.actualWhirl = (int) Math.round((double) actualWhirl / 100) * 100;
		else
			System.out.println("Nie ma takich obrotów");
	}
	
	public void upV() {
		if (this.actualWhirl >= 0 && this.actualWhirl < maxWhirl) {
			this.actualWhirl += 100;
		} else {
			this.actualWhirl = 0;
		}
	}
	
	public void downV() {
		if (this.actualWhirl > 0 && this.actualWhirl <= maxWhirl) {
			this.actualWhirl -= 100;
		} else {
			this.actualWhirl = maxWhirl;
		}
	}
	
	public void setProgram(int actualProg) {
		if (actualProg <= programMax && actualProg > 0) {
			this.actualProg = actualProg;
		} else {
			System.out.println("Nie ma takiego programu");
		}
	}
	
	public int getActualProg() {
		return actualProg;
	}
	
	public void nextProgram() {
		if (this.actualProg > 0 && this.actualProg < programMax) {
			this.actualProg++;
		} else {
			this.actualProg = 1;
		}
	}
	
	public void previousProgram() {
		if (this.actualProg > 1 && this.actualProg <= programMax) {
			this.actualProg--;
		} else {
			this.actualProg = programMax;
		}
	}
	
	public void setTemp(double temp) {
		if (temp > maxTemp) {
			this.temp = maxTemp;
			System.out.println("Przekroczono zakres max");
		} else if (temp < 0) {
			this.temp = 0;
			System.out.println("Przekroczono zakres min");
		} else {
			this.temp = Math.round(temp * variableSetup) / variableSetup;
		}
	}
	
	public double getTemp() {
		return temp;
	}
	
	public void tempUp() throws PralkaException {
		if (this.temp >= 0 && this.temp < maxTemp) {
			this.temp += tempRegulation;
			System.out.println("Aktualna temperatura wynosi: " + this.temp + "\u00B0C");
		} else {
			throw new PralkaException();
		}
	}
	
	public void tempDown() throws PralkaException {
		if (this.temp > 0 && this.temp <= maxTemp) {
			this.temp -= tempRegulation;
			System.out.println("Aktualna temperatura wynosi: " + this.temp + "\u00B0C");
		} else {
			throw new PralkaException();
		}
	}
	
	public void showStatus() {
		System.out.println("========================");
		System.out.println("Producent: " + this.producent);
		System.out.println("Aktualna temperatura: " + this.temp + "\u00B0C");
		System.out.println("Aktualne obroty: " + this.actualWhirl);
		System.out.println("Aktualny program: " + this.actualProg);
		System.out.println("Regulacja temperatury o: " + this.tempRegulation + "\u00B0C");
		System.out.println("Ilość programów: " + this.programMax);
		System.out.println("========================");
	}
	
	@Override
	public String toString() {
		return "Pralka [producent=" + producent + ", programMax=" + programMax + ", maxTemp=" + maxTemp + ", maxWhirl=" +
				maxWhirl + "]" + "\n";
	}
}
