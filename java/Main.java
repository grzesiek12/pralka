package pralka.java;

import java.util.ArrayList;
import java.util.List;

public class Main {
	
	public static void main(String[] args) {
		
		Beko beko = new Beko();
		Amica amica = new Amica();
		Wihrpool wihrpool = new Wihrpool();
		
		List<Pralka> lista = new ArrayList<Pralka>();
		lista.add(beko);
		lista.add(amica);
		lista.add(wihrpool);
		
		sortList(lista);
		
		beko.setProgram(20);
		beko.previousProgram();
		beko.setTemp(90);
		try {
			beko.tempDown();
		} catch (PralkaException e) {
			e.printStackTrace();
		}
		beko.setActualWhirl(460);
		beko.upV();
		beko.showStatus();
		
		amica.setProgram(20);
		amica.nextProgram();
		amica.setTemp(90);
		try {
			amica.tempUp();
		} catch (PralkaException e) {
			e.printStackTrace();
		}
		amica.setActualWhirl(820);
		amica.downV();
		amica.showStatus();
		
		wihrpool.setProgram(24);
		wihrpool.nextProgram();
		wihrpool.setTemp(50);
		try {
			wihrpool.tempDown();
		} catch (PralkaException e) {
			e.printStackTrace();
		}
		wihrpool.setActualWhirl(1000);
		wihrpool.upV();
		wihrpool.showStatus();
		
	}
	
	public static void sortList(List<Pralka> list) {
		System.out.println("===Przed sortowaniem===");
		list.stream()
				.forEach(System.out::print);
		
		System.out.println("===Po sortowaniu===");
		list.stream()
				.sorted((e2, e1) -> e2.getProducent()
						.name()
						.compareTo(e1.getProducent()
								.name()))
				.forEach(System.out::print);
		
	}
}
